# frozen_string_literal: true

Sidekiq::Logging.logger = Rails.logger

# TODO: Define `REDIS_PROVIDER` instead of settings this manually.
redis_url = ENV.fetch('REDIS_PROVIDER') { 'redis://localhost:6379/0' }
redis_config = { url: redis_url }

Sidekiq.configure_server do |config|
  config.redis = redis_config
end

Sidekiq.configure_client do |config|
  config.redis = redis_config
end
