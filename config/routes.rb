# frozen_string_literal: true

require 'sidekiq/web'

Rails.application.routes.draw do
  root to: 'pages#home'

  get '/import', to: 'import#index'
  post '/import', to: 'import#import'
  delete '/clean', to: 'import#delete'
  get '/import/files', to: 'import#files'
  get '/import/file', to: 'import#file'

  mount Sidekiq::Web => '/sidekiq'
end
