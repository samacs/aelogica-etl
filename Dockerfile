FROM ruby:2.3.1 AS builder

WORKDIR /bundler

RUN printf "deb http://archive.debian.org/debian/ jessie main\ndeb-src http://archive.debian.org/debian/ jessie main\ndeb http://security.debian.org jessie/updates main\ndeb-src http://security.debian.org jessie/updates main" > /etc/apt/sources.list

RUN apt-get update -qqy && \
    apt-get install -y --no-install-recommends nodejs \
                                               locales \
                                               sqlite3 \
                                               libsqlite3-dev

COPY ./lib/etl /bundler/lib/etl/

RUN gem update bundler

RUN rm -rf /var/lib/apt/lists/*

ENV RAILS_ENV=development \
    BUNDLE_PATH=/bundler \
    BUNDLE_BIN=${BUNDLE_PATH}/bin \
    GEM_HOME=${BUNDLE_PATH} \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8

RUN locale-gen en_US.UTF-8 \
    dpkg-reconfigure locales

COPY Gemfile Gemfile.lock ./

RUN bundle install

RUN cd lib/etl \
    bundle install

FROM builder

WORKDIR /app

ENV RAILS_ENV=development \
    BUNDLE_PATH=/bundler \
    BUNDLE_BIN=${BUNDLE_PATH}/bin \
    GEM_HOME=${BUNDLE_PATH} \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8

COPY . ./

RUN chmod +x /app/docker-entrypoint.sh

ENTRYPOINT ["/app/docker-entrypoint.sh"]

EXPOSE 3000

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]
