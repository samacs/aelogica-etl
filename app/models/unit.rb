# frozen_string_literal: true

# Unit model.
class Unit < ApplicationRecord
  default_scope { order(name: :asc) }
end
