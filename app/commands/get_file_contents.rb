# frozen_string_literal: true

# Reads file contents.
class GetFileContents
  prepend SimpleCommand
  include SyntaxHelper

  def initialize(filename, type: :json, lines: 100)
    @filename = filename
    @type = type.downcase
    @lines = lines
  end

  def call
    return highlight(read_file) if file_exist?

    errors.add(:file, "File #{filepath} does not exist.")
    nil
  end

  private

  attr_reader :filename, :type, :lines

  def read_file
    IO.foreach(filepath).take(@lines).join
  end

  def filepath
    @filepath ||= Rails.root.join('data', @type, @filename)
  end

  def file_exist?
    File.exist?(filepath)
  end
end
