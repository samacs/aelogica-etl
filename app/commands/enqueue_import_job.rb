# frozen_string_literal: true

# Enqueues an import job.
class EnqueueImportJob
  prepend SimpleCommand

  def initialize(context = {})
    context[:destination_options] ||= Etl::Configuration.destination_options
    context[:source] ||= Etl::Configuration.source
    context[:source] = context[:source].downcase
    context[:location] ||= Etl::Configuration.location
    context[:resource] ||= Etl::Configuration.resource
    context[:resource] = filepath_for(context[:resource], type: context[:source])
    context[:mappings] = Etl::Configuration.mappings[context[:source]]
    context[:mappings][:base_path][:from] = context[:base_path] unless
      context[:base_path].empty?

    context.delete(:base_path)

    @context = context
  end

  def call
    importer.perform_async(@context.to_json)
  end

  private

  attr_reader :context

  def importer
    @importer ||= Etl::Worker::Importer
  end

  def filepath_for(filename, type: :json)
    Rails.root.join('data', type, filename).to_s
  end
end
