# frozen_string_literal: true

# Loads data files for the given source.
class GetDataFiles
  prepend SimpleCommand

  def initialize(type)
    @type = type.downcase
  end

  def call
    files = Dir[Rails.root.join('data', @type, '**', "*.#{@type}")]
    files.map { |file| File.basename(file) }
  end

  private

  attr_reader :type
end
