# frozen_string_literal: true

# Controller for static pages.
class PagesController < ApplicationController
  def home
    @units = Unit.all
  end
end
