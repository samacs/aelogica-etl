# frozen_string_literal: true

# Imports data.
class ImportController < ApplicationController
  def index
    @sources = %i[json csv].map { |source| source.to_s.upcase }
  end

  def import
    command = EnqueueImportJob.call(import_params)
    return redirect_to root_path, success: 'Import job has been queued.' if
      command.success?

    redirect_to import_path, error: command.errors
  end

  def files
    command = GetDataFiles.call(files_params[:type])
    if command.success?
      render json: command.result
    else
      render json: { errors: command.errors },
             status: :internal_server_error
    end
  end

  def file
    command = GetFileContents.call(file_params[:name], type: file_params[:type])
    if command.success?
      render json: { contents: command.result }
    else
      render json: { errors: command.errors },
             status: :internal_server_error
    end
  end

  def delete
    units = Unit.count
    redirect_to root_path, success: "#{units} deleted." if Unit.delete_all
  end

  def import_params
    params.require(:import).permit(:source, :resource, :base_path)
  end

  def files_params
    params.require(:file).permit(:type)
  end

  def file_params
    params.require(:file).permit(:type, :name)
  end
end
