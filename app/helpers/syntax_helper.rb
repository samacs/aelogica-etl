# frozen_string_literal: true

# Used for syntax highlighting.
module SyntaxHelper
  def highlight(contents, language: :ruby)
    formatter = Rouge::Formatters::HTML.new(css_class: 'highlight solarized-dark')
    lexer = Rouge::Lexer.find(language)
    formatter.format(lexer.lex(contents))
  end
end
