# frozen_string_literal: true

require 'spec_helper'
require 'light-service/testing'

describe Etl::Service::Importer, type: :db do
  let(:successful_json_context) do
    json_context
  end

  let(:successful_csv_context) do
    csv_context
  end

  let(:failing_json_context_invalid_location) do
    json_context.merge(location: :invalid)
  end

  let(:failing_csv_context_invalid_location) do
    csv_context.merge(location: :invalid)
  end

  let(:failing_json_context_invalid_source) do
    json_context.merge(source: :invalid)
  end

  let(:failing_csv_context_invalid_source) do
    csv_context.merge(source: :invalid)
  end

  let(:failing_json_context_empty_source) do
    json_context.clone.except(:source)
  end

  let(:failing_csv_context_empty_source) do
    csv_context.clone.except(:source)
  end

  let(:failing_json_context_empty_database_url) do
    context = json_context.clone
    context[:destination_options][:options].delete(:database_url)
    context
  end

  let(:failing_csv_context_empty_database_url) do
    context = csv_context.clone
    context[:destination_options][:options].delete(:database_url)
    context
  end

  it 'should import data from a JSON file' do
    result = described_class.call(successful_json_context)
    expect(result).to be_success
    expect(dataset.count).to_not be_zero
  end

  it 'should import data from a CSV file' do
    result = described_class.call(successful_csv_context)
    expect(result).to be_success
    expect(dataset.count).to_not be_zero
  end

  it 'should fail when the location is not valid' do
    result = described_class.call(failing_json_context_invalid_location)
    expect(result).to_not be_success
  end

  it 'should fail when the locationn is not valid' do
    result = described_class.call(failing_csv_context_invalid_location)
    expect(result).to_not be_success
  end

  it 'should fail when source is not set' do
    expect do
      described_class.call(failing_json_context_empty_source)
    end.to raise_error ArgumentError
  end

  it 'should fail when source is not set' do
    expect do
      described_class.call(failing_csv_context_empty_source)
    end.to raise_error ArgumentError
  end

  it 'should fail when the JSON database options are not set' do
    expect do
      described_class.call(failing_json_context_empty_database_url)
    end.to raise_error ArgumentError
  end

  it 'should fail when the database options are not set' do
    expect do
      described_class.call(failing_csv_context_empty_database_url)
    end.to raise_error ArgumentError
  end
end
