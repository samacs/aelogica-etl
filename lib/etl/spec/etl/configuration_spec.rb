# frozen_string_literal: true

require 'spec_helper'

describe Etl::Configuration do
  it 'should load configuration from gem config' do
    expect(described_class.destination_options).to_not be_nil
    expect(described_class.mappings).to_not be_nil
    expect(described_class.destination_options.type).to eq 'database'
  end
end
