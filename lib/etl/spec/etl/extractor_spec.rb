# frozen_string_literal: true

require 'spec_helper'

describe Etl::Extractor do
  let(:object) { { a: { b: :c, d: { e: :f } } } }
  let(:collection) do
    { root: [{ a: { b: { c: { d: :f } } } },
             { a: { b: { c: { d: :f } } } },
             { a: { b: { c: { d: :f } } } }] }
  end

  it 'should extract data from an object' do
    mappings = { fields: { a: 'a.b' } }
    expected = { a: :c }
    actual = Etl::Extractor.extract(object, mappings)
    expect(actual).to eq expected
  end

  it 'should extract dadta from a collection' do
    mappings = { fields: { a: 'a.b' } }
    expected = [{ a: { c: { d: :f } } },
                { a: { c: { d: :f } } },
                { a: { c: { d: :f } } }]
    actual = Etl::Extractor.extract(collection[:root], mappings)
    expect(actual).to eq expected
  end

  it 'should extract data using a base path' do
    mappings = { base_path: { from: 'a', type: :object },
                 fields: { e: 'd.e' } }
    expected = { e: :f }
    actual = Etl::Extractor.extract(object, mappings)
    expect(actual).to eq expected
  end

  it 'should extract data from collection using base path' do
    mappings = { base_path: { from: 'root', type: :array },
                 fields: { e: 'a.b.c.d' } }
    expected = [{ e: :f }, { e: :f }, { e: :f }]
    actual = Etl::Extractor.extract(collection, mappings)
    expect(actual).to eq expected
  end
end
