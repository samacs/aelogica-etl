# frozen_string_literal: true

require 'spec_helper'

# TODO: Add example for MySQL database.

describe Etl::Destination::Database, type: :db do
  let(:destination) do
    options = { database_url: database_url,
                chunk_size: 20 }
    described_class.new(options)
  end
  let(:table_name) { :items }

  it 'should insert a single object' do
    item = { name: Faker::Name.name,
             price: Faker::Number.decimal(2) }
    destination.write(item, table_name)
    dataset = db[table_name]
    expect(dataset.count).to eq 1
  end

  it 'should insert a collection of objects' do
    items = []
    5.times do
      items << { name: Faker::Name.name,
                 price: Faker::Number.decimal(2) }
    end
    destination.write(items, table_name)
    dataset = db[table_name]
    expect(dataset.count).to eq 5
    items.each do |item|
      expect(dataset.all).to include(include(name: item[:name]))
    end
  end
end
