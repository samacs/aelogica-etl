# frozen_string_literal: true

require 'spec_helper'

describe Etl::Source::Csv, type: :db do
  it 'should fail when the `location` is not valid' do
    expect do
      Etl::Source::Csv.load(csv_filename, location: :invalid)
    end.to raise_error ArgumentError
  end

  it 'should fail when the `location` is `:url`' do
    expect do
      Etl::Source::Csv.load(csv_filename, location: :url)
    end.to raise_error NotImplementedError
  end

  it 'should load a CSV file' do
    contents = Etl::Source::Csv.load(csv_filename)
    expect(contents).to_not be_nil
    expect(contents.count).to_not be_zero
  end
end
