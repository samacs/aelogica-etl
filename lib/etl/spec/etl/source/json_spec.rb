# frozen_string_literal: true

require 'spec_helper'

describe Etl::Source::Json, type: :db do
  it 'should fail when the `location` is not valid' do
    expect do
      Etl::Source::Json.load(json_filename, location: :invalid)
    end.to raise_error ArgumentError
  end

  it 'should load a JSON file' do
    contents = Etl::Source::Json.load(json_filename)
    expect(contents).to_not be_nil
  end

  it 'should load JSON file from URL' do
    resource = 'http://ip.jsontest.com/'
    contents = Etl::Source::Json.load(resource, location: :url)
    expect(contents).to_not be_nil
  end
end
