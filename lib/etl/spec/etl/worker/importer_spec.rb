# frozen_string_literal: true

require 'spec_helper'

describe Etl::Worker::Importer, type: :db do
  it 'should enqueue an import job with JSON data' do
    expect do
      described_class.perform_async(json_context)
    end.to change(described_class.jobs, :size).by(1)
  end

  it 'should enqueue an import job with CSV data' do
    expect do
      described_class.perform_async(csv_context)
    end.to change(described_class.jobs, :size).by(1)
  end

  it 'should fire up the worker and import data' do
    expect(dataset.count).to be_zero
    described_class.new.perform(json_context)
    items = dataset.all
    expect(items.count).to_not be_zero
    json_data.each do |item|
      expect(items).to include(include(name: item[:name]))
    end
  end

  it 'should fire up the worker and import CSV data' do
    expect(dataset.count).to be_zero
    described_class.new.perform(csv_context)
    items = dataset.all
    expect(items.count).to_not be_zero
    csv_data.each do |item|
      expect(items).to include(include(name: item[:name]))
    end
  end
end
