# frozen_string_literal: true

require 'spec_helper'
require 'light-service/testing'

describe Etl::Action::WriteToDestination, type: :db do
  let(:context_factory) do
    LightService::Testing::ContextFactory
      .make_from(Etl::Service::Importer)
      .for(described_class)
  end

  let(:successful_json_context) do
    context_factory.with(json_context.clone.merge(data: json_data))
  end

  let(:successful_csv_context) do
    context_factory.with(csv_context.clone.merge(data: csv_data))
  end

  let(:failing_json_context_invalid_destination_type) do
    options = { database_url: database_url, table_name: table_name }
    destination_options = { type: :invalid,
                            options: options }
    context = json_context.clone.merge(destination_options: destination_options)
    context_factory.with(context)
  end

  let(:failing_csv_context_invalid_destination_type) do
    options = { database_url: database_url, table_name: table_name }
    destination_options = { type: :invalid,
                            options: options }
    context = csv_context.clone.merge(destination_options: destination_options)
    context_factory.with(context)
  end

  let(:dataset) { db[table_name] }

  it 'should write the JSON data to the destination' do
    result = described_class.execute(successful_json_context)
    expect(result).to be_success
    expect(dataset.count).to_not be_zero
    json_data.each do |item|
      expect(dataset.all).to include(include(name: item[:name]))
    end
  end

  it 'should write the CSV data to the destination' do
    result = described_class.execute(successful_csv_context)
    expect(result).to be_success
    expect(dataset.count).to_not be_zero
    csv_data.each do |item|
      expect(dataset.all).to include(include(name: item[:name]))
    end
  end

  it 'should fail when the destination type is not valid' do
    result = described_class.execute(failing_json_context_invalid_destination_type)
    expect(result).to_not be_success
  end
end
