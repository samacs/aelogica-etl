# frozen_string_literal: true

require 'spec_helper'
require 'light-service/testing'

describe Etl::Action::LoadFromSource, type: :db do
  let(:context_factory) do
    LightService::Testing::ContextFactory
      .make_from(Etl::Service::Importer)
      .for(described_class)
  end

  let(:successful_json_context) do
    context_factory.with(json_context)
  end

  let(:successful_csv_context) do
    context_factory.with(csv_context)
  end

  let(:failing_json_context_invalid_source) do
    context_factory.with(json_context.clone.merge(source: :invalid))
  end

  let(:failing_csv_context_invalid_source) do
    context_factory.with(csv_context.clone.merge(source: :invalid))
  end

  let(:failing_json_context_invalid_location) do
    context_factory.with(json_context.clone.merge(location: :invalid))
  end

  let(:failing_csv_context_invalid_location) do
    context_factory.with(csv_context.clone.merge(location: :invalid))
  end

  it 'should extract data from JSON source' do
    result = described_class.execute(successful_json_context)
    expect(result).to be_success
    expect(result.source).to eq :json
    expect(result.location).to eq :file
    expect(result.parsed_data).to_not be_nil
  end

  it 'should extract data from CSV source' do
    result = described_class.execute(successful_csv_context)
    expect(result).to be_success
    expect(result.source).to eq :csv
    expect(result.location).to eq :file
    expect(result.parsed_data).to_not be_nil
  end

  it 'should fail when the JSON location is not valid' do
    result = described_class.execute(failing_json_context_invalid_location)
    expect(result).to_not be_success
  end

  it 'should fail when the CSV location is not valid' do
    result = described_class.execute(failing_csv_context_invalid_location)
    expect(result).to_not be_success
  end

  it 'should fail when the JSON source type is not valid' do
    result = described_class.execute(failing_json_context_invalid_source)
    expect(result).to_not be_success
  end

  it 'should fail when the CSV source type is not valid' do
    result = described_class.execute(failing_csv_context_invalid_source)
    expect(result).to_not be_success
  end
end
