# frozen_string_literal: true

require 'spec_helper'
require 'light-service/testing'

# TODO: Add failing examples for when a key is not found.

describe Etl::Action::ExtractFromParsedData, type: :db do
  let(:context_factory) do
    LightService::Testing::ContextFactory
      .make_from(Etl::Service::Importer)
      .for(described_class)
  end

  let(:successful_json_context) do
    context_factory.with(json_context)
  end

  let(:successful_csv_context) do
    context_factory.with(csv_context)
  end

  it 'should extract data from parsed JSON data' do
    result = described_class.execute(successful_json_context)
    expect(result).to be_success
  end

  it 'should extract data from parsed CSV data' do
    result = described_class.execute(successful_csv_context)
    expect(result).to be_success
  end
end
