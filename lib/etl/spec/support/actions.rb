# frozen_string_literal: true

module ActionContext
  def json_data
    raw_data = Etl::Source::Json.load(json_filename, location: :file)
    Etl::Extractor.extract(raw_data, json_mappings)
  end

  def csv_data
    raw_data = Etl::Source::Csv.load(csv_filename, location: :file)
    Etl::Extractor.extract(raw_data, csv_mappings)
  end

  def json_mappings
    @json_mappings ||= { base_path: { from: 'unit_groups', type: :array },
                         fields: { name: 'name', price: 'price' } }
  end

  def csv_mappings
    @csv_mappings ||= { fields: { name: 'name', price: 'price' } }
  end

  def base_context
    return @base_context unless @base_context.nil?

    destination_options = { type: :database,
                            options: { database_url: database_url,
                                       table_name: :items } }
    @base_context ||= { location: :file,
                        destination_options: destination_options }
  end

  def json_context
    return @json_context unless @json_context.nil?

    context = { source: :json,
                resource: json_filename,
                mappings: json_mappings }
    base_context.merge(context)
  end

  def csv_context
    return @csv_context unless @csv_context.nil?

    context = { source: :csv,
                resource: csv_filename,
                mappings: csv_mappings }
    base_context.merge(context)
  end
end

RSpec.configure do |config|
  config.include ActionContext
end
