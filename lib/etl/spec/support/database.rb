# frozen_string_literal: true

module DatabaseInitializer
  def json_filename
    return @json_filename unless @json_filename.nil?

    @json_filename = File.join(specs_dir, 'data', 'json', 'unit_groups.json')
  end

  def csv_filename
    return @csv_filename unless @csv_filename.nil?

    @csv_filename = File.join(specs_dir, 'data', 'csv', 'units.csv')
  end

  def specs_dir
    @specs_dir ||= File.expand_path('..', __dir__)
  end

  def database_file
    return @database_file unless @database_file.nil?

    base_path = File.expand_path('..', __dir__)
    @database_file = File.join(base_path, 'test.sqlite')
  end

  def database_url
    @database_url ||= "sqlite://#{database_file}"
  end

  def db
    @db ||= Sequel.connect("sqlite://#{database_file}")
  end

  def dataset
    @dataset ||= db[table_name]
  end

  def table_name
    @table_name ||= :items
  end
end

RSpec.configure do |config|
  config.include DatabaseInitializer, type: :db

  config.before(:all, type: :db) do
    db.create_table? :items do
      primary_key :id
      String :name, size: 100
      BigDecimal :price, size: [8, 2]
      DateTime :created_at
      DateTime :updated_at
    end
  end

  config.before(:each, type: :db) do
    db.run('DELETE FROM items')
    db.run('UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name = "items"')
    db.run('VACUUM')
  end
end
