# frozen_string_literal: true

require 'etl/version'
require 'etl/source'
require 'etl/extractor'
require 'etl/destination'
require 'etl/service'
require 'etl/action'
require 'etl/worker'

# ETL extracts data from different sources
# to write it to different destinations.
module Etl
end
