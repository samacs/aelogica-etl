# frozen_string_literal: true

require 'settingslogic'

module Etl
  # Adds methods for finding the root directory
  # of the project.
  module ConfigurationMethods
    class << self
      def included(klass)
        klass.extend ClassMethods
      end
    end

    # Adds methods for finding the root directory
    # of the project.
    module ClassMethods
      private

      def configuration_file
        filename = project_root.join('config', 'etl.yml')
        return filename if File.exist?(filename)
      end

      def project_root
        return Rails.root if defined?(Rails)

        config_path = File.expand_path('../..', __dir__)
        Pathname.new(config_path)
      end

      def etl_namespace
        return Rails.env if defined?(Rails)

        ENV['ETL_ENV'] || 'development'
      end
    end
  end
end

module Etl
  # Configuration for ETL.
  class Configuration < Settingslogic
    include ConfigurationMethods

    source configuration_file
    namespace etl_namespace
  end
end
