# frozen_string_literal: true

require 'light-service'

module Etl
  module Action
    # Extracts data from parsed data source.
    class ExtractFromParsedData
      extend LightService::Action

      expects :parsed_data, :mappings
      promises :data

      executed do |context|
        contents = context.parsed_data
        mappings = context.mappings
        context.data = Etl::Extractor.extract(contents, mappings)
      end
    end
  end
end
