# frozen_string_literal: true

require 'light-service'

module Etl
  module Action
    # Writes extracted data to a destination.
    class WriteToDestination
      extend LightService::Action

      expects :data, :destination_options

      executed do |context|
        begin
          options = context.destination_options[:options]
          type = context.destination_options[:type]
          destination = case type.to_sym
                        when :database
                          Etl::Destination::Database.new(options)
                        else
                          message = "Destination type not supported: #{type}"
                          raise ArgumentError, message
                        end
          destination.write(context.data)
        rescue ArgumentError => e
          context.fail_and_return!(e.message)
        end
      end
    end
  end
end
