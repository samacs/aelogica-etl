# frozen_string_literal: true

require 'light-service'

module Etl
  module Action
    # Loads data from a source.
    class LoadFromSource
      extend LightService::Action

      expects :source, :resource, :location
      promises :parsed_data

      executed do |context|
        begin
          source = case context.source.to_sym
                   when :json
                     Etl::Source::Json
                   when :csv
                     Etl::Source::Csv
                   else
                     message = "Invalid source type: #{context.source}"
                     raise ArgumentError, message
                   end
          context.parsed_data = source.load(context.resource,
                                            location: context.location)
        rescue ArgumentError => e
          context.fail_and_return!(e.message)
        end
      end
    end
  end
end
