# frozen_string_literal: true

require 'sequel'

module Etl
  module Destination
    # Writes data to a database.
    class Database
      def initialize(opts = {})
        @opts = opts
      end

      def write(data, table_name = nil)
        table_name ||= @opts[:table_name].to_sym
        db.transaction do
          return insert_single(data, table_name) if data.is_a?(Hash)

          return insert_multiple(data, table_name) if data.is_a?(Array)
        end
      ensure
        db.disconnect
      end

      protected

      def insert_single(data, table_name)
        data[:created_at] = now
        data[:updated_at] = now
        db[table_name].insert(data)
      end

      def insert_multiple(data, table_name)
        chunk_size = @opts[:chunk_size] || 20
        data.each_slice(chunk_size) do |rows|
          rows.map do |row|
            row[:created_at] = now
            row[:updated_at] = now
          end
          db[table_name].multi_insert(rows)
        end
      end

      def db
        @db ||= Sequel.connect(db_options)
      end

      private

      def now
        Time.now
      end

      def db_options
        return @opts[:database_url] if @opts.key?(:database_url)

        @opts.extract!(%i[adapter host user password port database])
      end
    end
  end
end
