# frozen_string_literal: true

require 'sidekiq'

module Etl
  module Worker
    # Enqueues a worker for importing data
    class Importer
      include Sidekiq::Worker

      def perform(context)
        context = JSON.parse(context, symbolize_keys: true) if
          context.is_a?(String)
        context = context.deep_symbolize_keys if context.is_a?(Hash)
        result = Etl::Service::Importer.call(context)
        logger.error "Import failed: #{result.message}" if result.failure?
      end
    end
  end
end
