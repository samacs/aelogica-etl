# frozen_string_literal: true

require 'light-service'

module Etl
  module Service
    # Imports data from the configured source
    # to a specific destination.
    class Importer
      extend ::LightService::Organizer

      class << self
        def call(context)
          validate!(context)

          with(context).reduce(actions)
        end

        def actions
          [
            Etl::Action::LoadFromSource,
            Etl::Action::ExtractFromParsedData,
            Etl::Action::WriteToDestination
          ]
        end

        private

        def validate!(context)
          root_options.each do |key|
            raise ArgumentError, "Required option not found: #{key}" if
              context[key].nil?
          end

          destination_options.each do |key|
            path = "destination_options.#{key}"
            raise ArgumentError, "Required option not found: #{path}" if
              context[:destination_options][key].nil?
          end

          path = 'destination_options.table_name'
          raise ArgumentError, "Required option not found: #{path}" if
            context[:destination_options][:options][:table_name].nil?

          return if context[:destination_options][:options][:database_url]

          database_options.each do |key|
            path = "destination_options.options.#{key}"
            raise ArgumentError, "Required option not found: #{path}" if
              context[:destination_options][:options][key].nil?
          end
        end

        def root_options
          %i[source location resource mappings destination_options]
        end

        def destination_options
          %i[type options]
        end

        def database_options
          %i[adapter host user password database]
        end
      end
    end
  end
end
