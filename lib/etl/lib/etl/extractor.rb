# frozen_string_literal: true

require 'key_path'

module Etl
  # Extracts data from an object using mapping information.
  module Extractor
    module_function

    def extract(contents, mappings)
      return extract_from(contents, mappings, mappings[:base_path]) if
        mappings.key?(:base_path)

      return extract_from_collection(contents, mappings) if
        contents.is_a? Array

      return extract_from_object(contents, mappings) if
        contents.is_a? Hash
    end

    def extract_from(contents, mappings, base_path)
      from = base_path[:from]
      type = base_path[:type].to_sym
      contents = contents.value_at_keypath(from.to_keypath)

      return extract_from_object(contents, mappings) if
        type == :object

      return extract_from_collection(contents, mappings) if
        type == :array
    end

    def extract_from_object(contents, mappings)
      result = {}
      mappings[:fields].each do |key, path|
        result[key] = contents.value_at_keypath(path.to_keypath)
      end
      result
    end

    def extract_from_collection(contents, mappings)
      results = []
      contents.each do |record|
        result = {}
        mappings[:fields].each do |key, path|
          result[key] = record.value_at_keypath(path.to_keypath)
        end
        results << result
      end
      results
    end
  end
end
