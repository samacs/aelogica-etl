# frozen_string_literal: true

require 'etl/source/json'
require 'etl/source/csv'

module Etl
  # Defines multiple sources.
  module Source
  end
end
