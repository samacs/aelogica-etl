# frozen_string_literal: true

require 'csv'

module Etl
  module Source
    # Loads CSV data froma file.
    module Csv
      module_function

      def load(resource, location: :file)
        case location.to_sym
        when :file
          from_file(resource)
        when :url
          raise NotImplementedError, 'Reading from a URL is not implemented'
        else
          raise ArgumentError, "Invalid resource location: #{location}"
        end
      end

      def from_file(filename)
        results = []
        CSV.foreach(filename, headers: true) do |row|
          result = row.headers.each_with_object({}) do |key, hash|
            hash[key] = row[key]
          end
          results << result
        end
        results
      end
    end
  end
end
