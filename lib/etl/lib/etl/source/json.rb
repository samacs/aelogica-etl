# frozen_string_literal: true

require 'uri'
require 'net/http'
require 'yajl'

module Etl
  module Source
    # Loads JSON data from a file or URL.
    module Json
      module_function

      def load(resource, location: :file)
        case location.to_sym
        when :file
          from_file(resource)
        when :url
          from_url(resource)
        else
          raise ArgumentError, "Invalid resource location: #{location}"
        end
      end

      def from_file(filename)
        json = File.new(filename, 'r')
        parser = Yajl::Parser.new
        parser.parse(json)
      end

      def from_url(resource)
        parsed = nil
        on_parse_complete = ->(object) { parsed = object }
        uri = URI.parse(resource)
        request = Net::HTTP::Get.new(uri.request_uri)
        Net::HTTP.start(uri.host, uri.port) do |http|
          http.request request do |response|
            parser = Yajl::Parser.new
            parser.on_parse_complete = on_parse_complete
            response.read_body do |chunk|
              parser << chunk
            end
          end
        end
        parsed
      end
    end
  end
end
