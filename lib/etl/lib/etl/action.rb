# frozen_string_literal: true

require 'etl/action/load_from_source'
require 'etl/action/extract_from_parsed_data'
require 'etl/action/write_to_destination'

module Etl
  # Defines different actions.
  module Action
  end
end
