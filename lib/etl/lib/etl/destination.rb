# frozen_string_literal: true

require 'etl/destination/database'

module Etl
  # Defines different data destinations.
  module Destination
  end
end
