# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'etl/version'

Gem::Specification.new do |spec|
  spec.name          = 'etl'
  spec.version       = Etl::VERSION
  spec.authors       = ['AELOGICA']
  spec.email         = ['info@aelogica.com']

  spec.summary       = 'ETL client'
  spec.description   = 'ETL client gem for exercise'
  spec.homepage      = 'https://git.appexpress.io/open-source/etl-exercise'
  spec.license       = 'MIT'

  # Prevent pushing this gem to RubyGems.org.
  # To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section
  # to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = ''
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files         = Dir['**/*'].select { |file| File.file?(file) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|feature)/})
  spec.require_paths = ['lib']

  spec.add_dependency 'key_path', '1.2.0'
  spec.add_dependency 'light-service', '~> 0.11'
  spec.add_dependency 'sequel', '5.20.0'
  spec.add_dependency 'settingslogic', '2.0.8'
  spec.add_dependency 'sidekiq', '5.2.7'
  spec.add_dependency 'sqlite3', '1.3.13'
  spec.add_dependency 'yajl-ruby', '1.4.1'

  spec.add_development_dependency 'bundler', '~> 1.16'
  spec.add_development_dependency 'faker', '1.9.3'
  spec.add_development_dependency 'minitest', '~> 5.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rspec-simplecov'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'simplecov-console'
end
